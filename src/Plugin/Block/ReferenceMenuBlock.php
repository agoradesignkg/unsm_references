<?php

namespace Drupal\unsm_references\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Link;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\path_alias\AliasManagerInterface;
use Drupal\unsm_references\ReferenceRepositoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a references menu block for the sidebar.
 *
 * @Block(
 *   id = "references_menu",
 *   admin_label = @Translation("References menu block"),
 *   category = "Unsinn"
 * )
 */
class ReferenceMenuBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The reference repository.
   *
   * @var \Drupal\unsm_references\ReferenceRepositoryInterface
   */
  protected $referenceRepository;

  /**
   * The path alias manager.
   *
   * @var \Drupal\path_alias\AliasManagerInterface
   */
  protected $aliasManager;

  /**
   * Constructs a new ReferenceMenuBlock object.
   *
   * @param array $configuration
   *   The plugin configuration, i.e. an array with configuration values keyed
   *   by configuration option name. The special key 'context' may be used to
   *   initialize the defined contexts by setting it to an array of context
   *   values keyed by context names.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\unsm_references\ReferenceRepositoryInterface $reference_repository
   *   The reference repository.
   * @param \Drupal\path_alias\AliasManagerInterface $alias_manager
   *   The path alias manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ReferenceRepositoryInterface $reference_repository, AliasManagerInterface $alias_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->referenceRepository = $reference_repository;
    $this->aliasManager = $alias_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('unsm_references.reference_repository'),
      $container->get('path_alias.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $links = [];
    $overview_url = unsm_references_get_reference_overview_url();
    if ($overview_url) {
      $links[] = Link::fromTextAndUrl($this->t('All references'), $overview_url);
    }

    $categories_with_references = $this->referenceRepository->loadCategoriesHavingReferences();
    foreach ($categories_with_references as $category) {
      $name = $category->getName();
      $path = $category->toUrl()->getInternalPath();
      $path = '/' . $path;
      $path_alias = $this->aliasManager->getAliasByPath($path);
      $alias_parts = explode('/', $path_alias);
      $argument = end($alias_parts);
      $overview_url = unsm_references_get_reference_overview_url($argument);
      $links[] = Link::fromTextAndUrl($name, $overview_url);
    }

    if (empty($links)) {
      return [];
    }

    return [
      '#theme' => 'item_list',
      '#items' => $links,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    if (!empty($this->configuration['label'])) {
      return $this->configuration['label'];
    }

    return $this->t('References');
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return Cache::mergeContexts(parent::getCacheContexts(), ['languages:language_content']);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    return Cache::mergeTags(parent::getCacheTags(), [UNSM_REFERENCES_MENU_BLOCK_CACHE_TAG]);
  }

  /**
   * Sanitize the given term name for use as URL argument (contextual filter).
   *
   * @param $name
   *   The category name.
   *
   * @return string
   *   The sanitized category name.
   *
   * @deprecated
   *   This function is no longer needed, after we switched from using term
   *   names to use path aliases instead.
   */
  protected function sanitizeTermName($name) {
    return strtolower(str_replace(' ', '-', $name));
  }

}
