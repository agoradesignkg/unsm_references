<?php

namespace Drupal\unsm_references;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;

class ReferenceRepository implements ReferenceRepositoryInterface {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new ReferenceRepository object.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(Connection $connection, EntityTypeManagerInterface $entity_type_manager) {
    $this->connection = $connection;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * @inheritDoc
   */
  public function loadCategoriesHavingReferences() {
    $query = $this->connection->select('taxonomy_term_field_data', 'ttfd');
    $query->innerJoin('taxonomy_index', 'ti', 'ttfd.tid = %alias.tid');
    $query->innerJoin('node_field_data', 'nfd', '%alias.nid = ti.nid AND %alias.type=:type AND %alias.status=:status', [':type' => 'reference', ':status' => 1]);

    $query->fields('ttfd', ['tid']);
    $query->condition('ttfd.vid', 'trailer_categories');
    $query->orderby('ttfd.weight');
    $query->orderby('ttfd.name');
    $query->addTag('taxonomy_term_access');
    $query->condition('ttfd.default_langcode', 1);

    $result = $query->execute()->fetchAllAssoc('tid');
    $all_tids = array_keys($result);
    return $this->entityTypeManager->getStorage('taxonomy_term')->loadMultiple($all_tids);
  }

}
