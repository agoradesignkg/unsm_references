<?php

namespace Drupal\unsm_references;

interface ReferenceRepositoryInterface {

  /**
   * Returns a list of category (trailer_categories) terms used by references.
   *
   * Returns a list of category (trailer_categories) terms that are associated
   * with at least one active reference node.
   *
   * @return \Drupal\taxonomy\TermInterface[]
   *   A list of category (trailer_categories) terms that are associated with
   *   at least one active reference node.
   */
  public function loadCategoriesHavingReferences();

}
